const API = "https://ajax.test-danit.com/api/swapi/films";

sendRequest(API)
.then(response => response.json())
.then(data => {
  renderFilms(data);
  return data;
})
.catch(error => console.error(error));


function sendRequest(url) {
  return fetch(url);
}

function renderFilms(data) {
  data.forEach(({ characters, episodeId, name, openingCrawl }) => {
    const containerFilm = document.createElement('div');
    containerFilm.innerHTML = `
        <h2>Episode ${episodeId}: ${name}</h2>
        <div class="characters-loader"><span class="loader"></span></div>
        <ul class="characters-list"></ul>
        <p>${openingCrawl}</p>
      `;
    document.body.append(containerFilm);  
    
    showCharacters()

    function showCharacters() {
      const charactersList = containerFilm.querySelector('.characters-list');
      const loaderContainer = containerFilm.querySelector('.characters-loader');

      Promise.all(characters.map(url => fetch(url)
        .then(response => response.json())))
        .then(characters => {
          charactersList.innerHTML = characters.map(character => `<li>${character.name}</li>`).join('');
          loaderContainer.style.display = 'none';
        })
  
        .catch(error => {
          console.error(error);
          charactersList.innerHTML = '<li>Error loading characters</li>';
          loaderContainer.style.display = 'none';
        });
    }
  }); 
}